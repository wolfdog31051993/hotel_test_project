import 'package:flutter/material.dart';

class ThemeColors {
  static const primaryColor = Color(0xFF0D72FF);
  static const secondaryPrimaryColor = Color(0x1A0D72FF);
  static const textColor = Colors.black;
  static const secondTextColor = Color(0xFF828796);
  static const goldColor = Color(0xFFFFA800);
  static const secondaryGoldColor = Color(0x33FFC700);
  static const whiteColor = Colors.white;
  static const backgroundColor = Color(0xFFE8E9EC);
  static const blackColor = Colors.black;
  static const infoContainerColor = Color(0xFFFBFBFC);
  static const errorColor = Color.fromARGB(255, 233, 48, 48);
}
