import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../app.dart';

class ConstantsStyle {
  static final formTextStyle = TextStyle(
    color: ThemeColors.blackColor,
    fontSize: 16.sp,
    fontWeight: FontWeight.w300,
  );
  static final formLabelTextStyle = TextStyle(
    color: ThemeColors.secondTextColor,
    fontSize: 16.sp,
    fontWeight: FontWeight.w300,
  );

  static Padding screenPadding = const Padding(padding: EdgeInsets.all(16));

  static final formHeaderStyle = TextStyle(fontSize: 14.sp, fontWeight: FontWeight.bold);
}
