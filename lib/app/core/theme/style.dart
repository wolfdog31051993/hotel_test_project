import 'package:flutter/material.dart';
import 'package:test_otel/app/core/theme/theme.dart';
import 'package:test_otel/app/core/util/constants.dart';

ThemeData themeData = ThemeData(
  primaryColor: ThemeColors.primaryColor,
  appBarTheme: const AppBarTheme(
    backgroundColor: ThemeColors.whiteColor,
    elevation: 0,
    scrolledUnderElevation: 1.5,
  ),
  scaffoldBackgroundColor: ThemeColors.backgroundColor,
  fontFamily: Constants.fontFamily,
);
