class HotelDetailModel {
  final List<String> images;
  final String name;
  final List<String> services;
  final String price;

  HotelDetailModel({required this.images, required this.name, required this.services, required this.price});
}
