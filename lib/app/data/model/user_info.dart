class UserInfoModel {
  final String name;
  final String nameCtr;
  final String surnameCtr;
  final String birthdayCtr;
  final String citizenshipCtr;
  final String passportNumCtr;
  final String passportDateEndCtr;

  UserInfoModel(
      {required this.name,
      required this.nameCtr,
      required this.surnameCtr,
      required this.birthdayCtr,
      required this.citizenshipCtr,
      required this.passportNumCtr,
      required this.passportDateEndCtr});
}
