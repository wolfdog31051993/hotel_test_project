import 'package:get/get.dart';
import 'package:test_otel/app/screen/home/home.dart';

import 'app_routes.dart';

class AppPages {
  static var list = [
    // GetPage(name: AppRoutes.initializer, page: () => const Initializer(), binding: InitializerBinding()),

    GetPage(name: AppRoutes.home, page: () => const HomeScreen()),
  ];
}
