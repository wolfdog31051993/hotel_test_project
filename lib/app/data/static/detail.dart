import 'package:test_otel/app/data/data.dart';

class StaticHotel {
  static List<HotelDetailModel> detailList = [
    HotelDetailModel(
        images: [
          'https://assets.hrewards.com/assets/jpg.xxlarge_551_SHR_Makadi_rooms_Superior_Golf_Queen_Bed_2_9a01236a57.jpg?optimize',
          'https://assets.hrewards.com/assets/SHR_Makadi_Superior_Sea_Twin_Bed_SUT_25efa4fe5b.jpg',
          'https://cf.bstatic.com/xdata/images/hotel/max1024x768/445406057.jpg?k=e34c9efdf82ec9bda4f7fbdcc41a98be3c187b4bfae8b667376aa9842b2b5471&o=&hp=1'
        ],
        name: 'Стандартный с видом на бассейн или сад',
        services: ['Все включено', 'Кондиционер'],
        price: '186 600 ₽'),
    HotelDetailModel(
        images: [
          'https://assets.hrewards.com/assets/jpg.xxlarge_551_SHR_Makadi_rooms_Superior_Golf_Queen_Bed_2_9a01236a57.jpg?optimize',
          'https://assets.hrewards.com/assets/SHR_Makadi_Superior_Sea_Twin_Bed_SUT_25efa4fe5b.jpg',
          'https://cf.bstatic.com/xdata/images/hotel/max1024x768/445406057.jpg?k=e34c9efdf82ec9bda4f7fbdcc41a98be3c187b4bfae8b667376aa9842b2b5471&o=&hp=1'
        ],
        name: 'Стандартный с видом на бассейн или сад',
        services: ['Все включено', 'Кондиционер'],
        price: '110 600 ₽'),
    HotelDetailModel(
        images: [
          'https://assets.hrewards.com/assets/jpg.xxlarge_551_SHR_Makadi_rooms_Superior_Golf_Queen_Bed_2_9a01236a57.jpg?optimize',
          'https://assets.hrewards.com/assets/SHR_Makadi_Superior_Sea_Twin_Bed_SUT_25efa4fe5b.jpg',
          'https://cf.bstatic.com/xdata/images/hotel/max1024x768/445406057.jpg?k=e34c9efdf82ec9bda4f7fbdcc41a98be3c187b4bfae8b667376aa9842b2b5471&o=&hp=1'
        ],
        name: 'Стандартный с видом на бассейн или сад',
        services: ['Все включено', 'Кондиционер'],
        price: '286 600 ₽'),
  ];
}
