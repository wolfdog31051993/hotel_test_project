import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../app.dart';

class MyTextFormField extends StatelessWidget {
  final TextEditingController controller;
  final String label;
  final String? hintText;
  final int maxLines;
  final bool obscureText;
  final bool isPhone;
  final bool autofocus;
  final TextInputType inputType;
  final Widget? suffix;
  final Widget? prefix;
  final String? prefixText;
  final FocusNode? focusNode;
  final List<TextInputFormatter>? inputFormatters;
  final Function(String value)? onChangeCallback;
  final Function(String? value)? onValidate;
  final VoidCallback? onTap;
  final Function(String)? onSelected;

  final bool readOnly;

  const MyTextFormField({
    super.key,
    required this.controller,
    required this.label,
    this.focusNode,
    this.hintText,
    this.maxLines = 1,
    this.obscureText = false,
    this.isPhone = false,
    this.autofocus = false,
    this.inputType = TextInputType.text,
    this.suffix,
    this.prefix,
    this.prefixText,
    this.inputFormatters,
    this.onChangeCallback,
    this.onValidate,
    this.onTap,
    this.onSelected,
    this.readOnly = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 52.h,
      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 16),
      decoration:
          const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10)), color: ThemeColors.backgroundColor),
      child: TextFormField(
        autofocus: autofocus,
        focusNode: focusNode,
        obscureText: obscureText,
        controller: controller,
        maxLines: maxLines,
        cursorColor: ThemeColors.blackColor,
        style: ConstantsStyle.formTextStyle,
        keyboardType: inputType,
        inputFormatters: inputFormatters != null ? [...inputFormatters!] : [],
        textInputAction: TextInputAction.done,
        readOnly: readOnly,
        decoration: inputDecoration(),
        validator: (value) => onValidate != null ? onValidate!(value) : _validator(value),
        onChanged: (value) => onChangeCallback != null ? onChangeCallback!(value) : null,
        onTap: onTap,
      ),
    );
  }

  InputDecoration inputDecoration() {
    return InputDecoration(
      labelText: label,
      labelStyle: ConstantsStyle.formLabelTextStyle,
      isDense: true,
      alignLabelWithHint: maxLines != 1,
      hintText: hintText,
      prefixText: prefixText,
      hintStyle: ConstantsStyle.formTextStyle,
      prefixIcon: prefix,
      suffixIcon: suffix,
      errorStyle: TextStyle(
        fontSize: 12.sp,
        color: ThemeColors.errorColor,
      ),
      suffixIconConstraints: const BoxConstraints(),
      border: InputBorder.none,
    );
  }

  String? _validator(String? value) {
    // if (obscureText) {
    //   return (value == null || value.trim().length < 6) ? 'validation_min_6_char'.tr : null;
    // }
    if (isPhone) {
      final phone = value?.replaceAll(RegExp(r'\D+'), '');
      return (phone == null || phone.trim().length < 8) ? 'validation_invalid_phone'.tr : null;
    }

    return (value == null || value.trim().isEmpty) ? '$label ${'validation_is_required'.tr}' : null;
  }
}
