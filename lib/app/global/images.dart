class MyAssetImages {
  static const closeIc = 'assets/icons/close_square.svg';
  static const emojiHappyIc = 'assets/icons/emoji_happy.svg';
  static const successIc = 'assets/icons/tick_square.svg';

  static const successImg = 'assets/image/success.png';
}
