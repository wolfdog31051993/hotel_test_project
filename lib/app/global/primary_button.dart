import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../app.dart';

class PrimaryButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;
  final double? width;
  final Color? color;
  final Widget? icon;
  const PrimaryButton({
    super.key,
    required this.onPressed,
    required this.text,
    this.width = double.infinity,
    this.color = ThemeColors.primaryColor,
    this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 55,
      width: width,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.all(
          Radius.circular(15.r),
        ),
      ),
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          elevation: 2.5,
          splashFactory: InkRipple.splashFactory,
          backgroundColor: color,
          shadowColor: ThemeColors.blackColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        ),
        child: icon == null
            ? Text(
                text,
                style: TextStyle(
                  fontFamily: Constants.fontFamily,
                  fontWeight: FontWeight.w400,
                  fontSize: 16.sp,
                  color: ThemeColors.whiteColor,
                ),
                textAlign: TextAlign.center,
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  icon!,
                  const SizedBox(width: 8),
                  Flexible(
                    child: Text(
                      text,
                      style: TextStyle(
                        fontFamily: Constants.fontFamily,
                        fontWeight: FontWeight.w400,
                        fontSize: 16.sp,
                        color: ThemeColors.whiteColor,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
