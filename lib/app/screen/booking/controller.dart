import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:test_otel/app/screen/booking/booking.dart';
import 'package:test_otel/app/screen/booking/elements/users_item.dart';

class BookingController extends GetxController {
  final state = BookingState();

  void onAddUserField() {
    debugPrint('onAddUserField');
    state.userInfList.add(const UserInfoItem(index: 0));
  }

  Future<void> showDate(context, TextEditingController controller) async {
    DateTime? pickedDate = await showDatePicker(
        context: context, initialDate: DateTime.now(), firstDate: DateTime(1900), lastDate: DateTime(2101));

    if (pickedDate != null) {
      debugPrint('$pickedDate');
      String formattedDate = DateFormat('yyyy-MM-dd').format(pickedDate);
      debugPrint(formattedDate);

      controller.text = formattedDate;
    } else {
      debugPrint("Date is not selected");
    }
  }
}
