import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../app.dart';

class AddUserButton extends StatelessWidget {
  const AddUserButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(12), topRight: Radius.circular(12)),
            color: ThemeColors.whiteColor),
        padding: const EdgeInsets.all(16.0),
        margin: const EdgeInsets.only(bottom: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Добавить туриста',
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 22.sp, fontStyle: FontStyle.normal),
            ),
            InkWell(
              onTap: () => Get.put(BookingController()).onAddUserField(),
              child: Container(
                padding: const EdgeInsets.all(10),
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(6)), color: ThemeColors.primaryColor),
                child: const Icon(
                  Icons.add,
                  color: ThemeColors.whiteColor,
                  size: 20,
                ),
              ),
            )
          ],
        ));
  }
}
