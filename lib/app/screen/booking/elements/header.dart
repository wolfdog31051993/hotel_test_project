import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../app.dart';

class BookingHeader extends StatelessWidget {
  const BookingHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.all(16.0),
      width: double.infinity,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(12)),
        color: ThemeColors.whiteColor,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 160.w,
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
                color: ThemeColors.secondaryGoldColor),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5),
              child: Row(
                children: [
                  const Icon(
                    Icons.star,
                    color: ThemeColors.goldColor,
                  ),
                  const SizedBox(width: 5),
                  Text(
                    '5 Превосходно',
                    style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w500, color: ThemeColors.goldColor),
                  )
                ],
              ),
            ),
          ),
          SizedBox(height: 8.h),
          Text(
            'Steigenberger Makadi',
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 22.sp, fontStyle: FontStyle.normal),
          ),
          SizedBox(height: 8.h),
          Text(
            'Madinat Makadi, Safaga Road, Makadi Bay, Египет',
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 14.sp,
                fontStyle: FontStyle.normal,
                color: ThemeColors.primaryColor),
          ),
        ],
      ),
    );
  }
}
