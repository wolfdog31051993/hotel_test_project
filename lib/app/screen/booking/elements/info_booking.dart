import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../app.dart';

class BookingInfo extends StatelessWidget {
  const BookingInfo({super.key});

  @override
  Widget build(BuildContext context) {
    SizedBox height = SizedBox(height: 16.h);
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      padding: const EdgeInsets.all(16.0),
      width: double.infinity,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(12)),
        color: ThemeColors.whiteColor,
      ),
      child: Column(
        children: [
          infoRow('Вылет из', 'Санкт-Петербург'),
          height,
          infoRow('Страна, город', 'Египет, Хургада'),
          height,
          infoRow('Даты', '19.09.2023 – 27.09.2023'),
          height,
          infoRow('Кол-во ночей', '7 ночей'),
          height,
          infoRow('Отель', 'Steigenberger Makadi'),
          height,
          infoRow('Номер', 'Стандартный с видом на бассейн или сад'),
          height,
          infoRow('Питание', 'Все включено'),
        ],
      ),
    );
  }

  Row infoRow(String tittle, String desc) {
    return Row(
      children: [
        SizedBox(
          width: 0.3.sw,
          child: Text(
            tittle,
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 16.sp,
                fontStyle: FontStyle.normal,
                color: ThemeColors.secondTextColor),
          ),
        ),
        SizedBox(
          width: 0.5.sw,
          child: Text(
            desc,
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 16.sp,
                fontStyle: FontStyle.normal,
                color: ThemeColors.textColor),
          ),
        )
      ],
    );
  }
}
