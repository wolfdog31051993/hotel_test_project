import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_otel/app/app.dart';
import 'package:test_otel/app/global/form.dart';

class InfoBuyer extends StatelessWidget {
  final BookingController bc;
  const InfoBuyer({super.key, required this.bc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      padding: const EdgeInsets.all(16.0),
      width: double.infinity,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(12)),
        color: ThemeColors.whiteColor,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Информация о покупателе',
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 22.sp,
                fontStyle: FontStyle.normal,
                color: ThemeColors.textColor),
          ),
          SizedBox(height: 20.h),
          MyTextFormField(
            controller: bc.state.phoneCtr,
            label: 'Номер телефона',
            inputType: TextInputType.phone,
          ),
          SizedBox(height: 8.h),
          MyTextFormField(
            controller: bc.state.emailCtr,
            label: 'Почта',
            inputType: TextInputType.emailAddress,
          ),
          SizedBox(height: 8.h),
          Text(
            'Эти данные никому не передаются. После оплаты мы вышли чек на указанный вами номер и почту',
            style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 14.sp,
                fontStyle: FontStyle.normal,
                color: ThemeColors.secondTextColor),
          ),
        ],
      ),
    );
  }
}
