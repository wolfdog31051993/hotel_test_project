import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../app.dart';

class BookingTotalPrice extends StatelessWidget {
  const BookingTotalPrice({super.key});

  @override
  Widget build(BuildContext context) {
    SizedBox height = SizedBox(height: 16.h);
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      padding: const EdgeInsets.all(16.0),
      width: double.infinity,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(12)),
        color: ThemeColors.whiteColor,
      ),
      child: Column(
        children: [
          infoRow('Тур', '186 600 ₽'),
          height,
          infoRow('Топливный сбор', '9 300 ₽'),
          height,
          infoRow('Сервисный сбор', '2 136 ₽'),
          height,
          infoRow('К оплате', '198 036 ₽', colors: ThemeColors.primaryColor),
        ],
      ),
    );
  }

  Row infoRow(String tittle, String desc, {Color colors = ThemeColors.textColor}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          tittle,
          style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 16.sp,
              fontStyle: FontStyle.normal,
              color: ThemeColors.secondTextColor),
        ),
        Text(
          desc,
          style: TextStyle(
            fontWeight: colors == ThemeColors.primaryColor ? FontWeight.w600 : FontWeight.w500,
            fontSize: 16.sp,
            fontStyle: FontStyle.normal,
            color: colors,
          ),
        )
      ],
    );
  }
}
