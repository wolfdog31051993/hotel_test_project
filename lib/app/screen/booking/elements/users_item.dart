import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:test_otel/app/app.dart';

class UserInfoItem extends StatelessWidget {
  final int index;

  const UserInfoItem({super.key, required this.index});

  @override
  Widget build(BuildContext context) {
    TextEditingController nameCtr = TextEditingController();
    TextEditingController surnameCtr = TextEditingController();
    TextEditingController birthdayCtr = TextEditingController();
    TextEditingController citizenshipCtr = TextEditingController();
    TextEditingController passportNumCtr = TextEditingController();
    TextEditingController passportDateEndCtr = TextEditingController();

    String tittle = '';
    switch (index) {
      case 0:
        tittle = 'Первый турист';
        break;
      case 1:
        tittle = 'Второй турист';
        break;
      case 2:
        tittle = 'Третий турист';
        break;
      case 3:
        tittle = 'Четвертый турист';
        break;
      case 4:
        tittle = 'Пятый турист';
        break;
      case 5:
        tittle = 'Шестой турист';
        break;
      case 6:
        tittle = 'Седьмой турист';
        break;
      default:
    }

    SizedBox height = SizedBox(height: 8.h);

    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      padding: const EdgeInsets.all(5.0),
      width: double.infinity,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(12)),
        color: ThemeColors.whiteColor,
      ),
      child: ExpansionTile(
        childrenPadding: EdgeInsets.zero,
        title: Text(
          tittle,
          style: TextStyle(fontWeight: FontWeight.w500, fontSize: 22.sp, fontStyle: FontStyle.normal),
        ),
        trailing: Container(
          padding: const EdgeInsets.all(10),
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(6)), color: ThemeColors.secondaryPrimaryColor),
          child: const Icon(
            Icons.keyboard_arrow_down,
            color: ThemeColors.primaryColor,
            size: 20,
          ),
        ),
        children: [
          GetBuilder<BookingController>(
              init: BookingController(),
              builder: (bc) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    children: [
                      MyTextFormField(controller: nameCtr, label: 'Имя'),
                      height,
                      MyTextFormField(controller: surnameCtr, label: 'Фамилия'),
                      height,
                      MyTextFormField(
                        controller: birthdayCtr,
                        label: 'Дата рождения',
                        readOnly: true,
                        onTap: () => bc.showDate(context, birthdayCtr),
                      ),
                      height,
                      MyTextFormField(controller: citizenshipCtr, label: 'Гражданство'),
                      height,
                      MyTextFormField(controller: passportNumCtr, label: 'Номер загранпаспорта'),
                      height,
                      MyTextFormField(
                        controller: passportDateEndCtr,
                        label: 'Срок действия загранпаспорта',
                        readOnly: true,
                        onTap: () => bc.showDate(context, passportDateEndCtr),
                      ),
                      height,
                    ],
                  ),
                );
              })
        ],
      ),
    );
  }
}
