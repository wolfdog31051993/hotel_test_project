import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:test_otel/app/screen/booking/elements/add.button_widget.dart';
import 'package:test_otel/app/screen/booking/elements/total_price.dart';
import 'package:test_otel/app/screen/booking/elements/users_item.dart';
import 'package:test_otel/app/screen/pay_success/screen.dart';

import '../../app.dart';

class BookingScreen extends StatelessWidget {
  const BookingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GetX<BookingController>(
      init: BookingController(),
      builder: (bc) {
        return Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            title: Text(
              'Бронирование',
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18.sp, color: ThemeColors.blackColor),
              textAlign: TextAlign.center,
            ),
            centerTitle: true,
            leading: IconButton(
              onPressed: () => Get.back(),
              icon: const Icon(
                Icons.arrow_back_ios,
                color: ThemeColors.blackColor,
              ),
            ),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                const BookingHeader(),
                const BookingInfo(),
                InfoBuyer(bc: bc),
                ListView.builder(
                    itemCount: bc.state.userInfList.length,
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return UserInfoItem(index: index++);
                    }),
                const AddUserButton(),
                const BookingTotalPrice(),
                Container(
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(12), topRight: Radius.circular(12)),
                      color: ThemeColors.whiteColor),
                  padding: const EdgeInsets.all(16.0),
                  child: PrimaryButton(
                      onPressed: () => Get.to(() => const PaySuccessScreen()), text: 'Оплатить 198 036 ₽'),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
