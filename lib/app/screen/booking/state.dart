import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class BookingState {
  TextEditingController phoneCtr = TextEditingController();
  TextEditingController emailCtr = TextEditingController();

  List<TextEditingController> nameCtr = <TextEditingController>[];
  List<TextEditingController> surnameCtr = <TextEditingController>[];
  List<TextEditingController> birthdayCtr = <TextEditingController>[];
  List<TextEditingController> citizenshipCtr = <TextEditingController>[];
  List<TextEditingController> passportNumCtr = <TextEditingController>[];
  List<TextEditingController> passportDateEndCtr = <TextEditingController>[];

  RxList<Widget> userInfList = <Widget>[].obs;
}
