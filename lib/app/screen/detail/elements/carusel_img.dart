import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:test_otel/app/app.dart';

class DetailCarouselImg extends StatefulWidget {
  final HotelDetailModel detail;
  const DetailCarouselImg({super.key, required this.detail});

  @override
  State<DetailCarouselImg> createState() => _DetailCarouselImgState();
}

class _DetailCarouselImgState extends State<DetailCarouselImg> {
  int pageIndicator = 0;
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DetailController>(
      init: DetailController(),
      builder: (hc) {
        return Stack(
          children: [
            SizedBox(
              height: 257.h,
              child: CarouselSlider.builder(
                options: CarouselOptions(
                  height: 400,
                  autoPlay: true,
                  autoPlayInterval: const Duration(seconds: 5),
                  viewportFraction: 1,
                  enlargeCenterPage: true,
                  onPageChanged: (value, _) => setState(
                    () => pageIndicator = value,
                  ),
                ),
                itemCount: widget.detail.images.length,
                itemBuilder: (context, index, pageViewIndex) {
                  // hc.combineBannerList();
                  return SizedBox(
                    height: 257.h,
                    width: 347.w,
                    child: NetworkImageWithLoader(widget.detail.images[index]),
                  );
                },
              ),
            ),
            Positioned(
              bottom: 15,
              width: MediaQuery.of(context).size.width,
              child: Align(
                alignment: Alignment.center,
                child: Container(
                  padding: const EdgeInsets.all(5),
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    color: ThemeColors.whiteColor,
                  ),
                  child: AnimatedSmoothIndicator(
                    activeIndex: pageIndicator,
                    count: hc.state.imagesList.length,
                    effect: ExpandingDotsEffect(
                      spacing: 5,
                      radius: 50.0,
                      dotWidth: 8.0,
                      dotHeight: 8.0.h,
                      activeDotColor: ThemeColors.blackColor,
                    ),
                  ),
                ),
              ),
            )
          ],
        );
      },
    );
  }
}
