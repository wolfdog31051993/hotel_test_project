import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:test_otel/app/app.dart';

import 'carusel_img.dart';

class DetailItem extends StatelessWidget {
  final HotelDetailModel detail;
  const DetailItem({super.key, required this.detail});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.all(16.0),
      width: double.infinity,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(12)),
        color: ThemeColors.whiteColor,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          DetailCarouselImg(
            detail: detail,
          ),
          SizedBox(height: 8.h),
          Text(
            detail.name,
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 22.sp, fontStyle: FontStyle.normal),
          ),
          Wrap(
            children: detail.services
                .map(
                  (f) => Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                    margin: const EdgeInsets.only(right: 5.0, top: 8, bottom: 10.0),
                    decoration: const BoxDecoration(
                      color: ThemeColors.infoContainerColor,
                      borderRadius: BorderRadius.all(
                        Radius.circular(5),
                      ),
                    ),
                    child: Text(
                      f,
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 16.sp,
                          fontStyle: FontStyle.normal,
                          color: ThemeColors.secondTextColor),
                    ),
                  ),
                )
                .toList(),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
            constraints: BoxConstraints(maxWidth: 180.w),
            decoration: const BoxDecoration(
              color: ThemeColors.secondaryPrimaryColor,
              borderRadius: BorderRadius.all(
                Radius.circular(5),
              ),
            ),
            child: Row(
              children: [
                Text(
                  'Подробнее о номере',
                  style: TextStyle(
                    color: ThemeColors.primaryColor,
                    fontSize: 16.sp,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ),
                ),
                SizedBox(width: 5.w),
                const Icon(
                  Icons.arrow_forward_ios,
                  color: ThemeColors.primaryColor,
                  size: 15,
                )
              ],
            ),
          ),
          SizedBox(height: 16.h),
          Row(
            children: [
              Text(
                detail.price,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 30.sp,
                  fontStyle: FontStyle.normal,
                  color: ThemeColors.blackColor,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 12.0, left: 8),
                child: Text(
                  'за 7 ночей с перелётом',
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 16.sp,
                    fontStyle: FontStyle.normal,
                    color: ThemeColors.secondTextColor,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 16.h),
          PrimaryButton(onPressed: () => Get.to(() => const BookingScreen()), text: 'Выбрать номер')
        ],
      ),
    );
  }
}
