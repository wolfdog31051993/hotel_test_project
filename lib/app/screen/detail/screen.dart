import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:test_otel/app/screen/detail/elements/item.dart';
import '/app/app.dart';

class DetailScreen extends StatelessWidget {
  final String tittle;
  const DetailScreen({super.key, required this.tittle});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DetailController>(
      init: DetailController(),
      builder: (dc) {
        return Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            title: Text(
              tittle,
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18.sp, color: ThemeColors.blackColor),
              textAlign: TextAlign.center,
            ),
            centerTitle: true,
            leading: IconButton(
              onPressed: () => Get.back(),
              icon: const Icon(
                Icons.arrow_back_ios,
                color: ThemeColors.blackColor,
              ),
            ),
          ),
          body: ListView.builder(
              shrinkWrap: true,
              physics: const BouncingScrollPhysics(),
              itemCount: StaticHotel.detailList.length,
              itemBuilder: (context, index) {
                return DetailItem(
                  detail: StaticHotel.detailList[index],
                );
              }),
        );
      },
    );
  }
}
