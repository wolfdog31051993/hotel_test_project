import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_otel/app/core/core.dart';
import 'package:test_otel/app/screen/home/elements/services_card.dart';
import 'package:test_otel/app/screen/home/home.dart';

class HomeAboutHotel extends StatelessWidget {
  final HomeController hc;
  const HomeAboutHotel({super.key, required this.hc});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(12)),
        color: ThemeColors.whiteColor,
      ),
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Об отеле',
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 22.sp, fontStyle: FontStyle.normal),
          ),
          Wrap(
            children: hc.state.aboutHotelList
                .map(
                  (f) => Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                    margin: const EdgeInsets.only(right: 5.0, top: 8, bottom: 10.0),
                    decoration: const BoxDecoration(
                      color: ThemeColors.infoContainerColor,
                      borderRadius: BorderRadius.all(
                        Radius.circular(5),
                      ),
                    ),
                    child: Text(
                      f,
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 16.sp,
                          fontStyle: FontStyle.normal,
                          color: ThemeColors.secondTextColor),
                    ),
                  ),
                )
                .toList(),
          ),
          Text(
            hc.state.customDesc,
            style: TextStyle(
              fontWeight: FontWeight.w400,
              color: ThemeColors.blackColor,
              fontSize: 16.sp,
            ),
          ),
          SizedBox(height: 16.0.h),
          const HomeServiceCard()
        ],
      ),
    );
  }
}
