import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:test_otel/app/app.dart';
import 'package:test_otel/app/screen/home/home.dart';

class HomeCarouselImg extends StatefulWidget {
  const HomeCarouselImg({super.key});

  @override
  State<HomeCarouselImg> createState() => _HomeCarouselImgState();
}

class _HomeCarouselImgState extends State<HomeCarouselImg> {
  int pageIndicator = 0;
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder: (hc) {
        return Stack(
          children: [
            SizedBox(
              height: 257.h,
              child: CarouselSlider.builder(
                options: CarouselOptions(
                  height: 400,
                  autoPlay: true,
                  autoPlayInterval: const Duration(seconds: 5),
                  viewportFraction: 1,
                  enlargeCenterPage: true,
                  onPageChanged: (value, _) => setState(
                    () => pageIndicator = value,
                  ),
                ),
                itemCount: hc.state.imagesList.length,
                itemBuilder: (context, index, pageViewIndex) {
                  // hc.combineBannerList();
                  return SizedBox(
                    height: 257.h,
                    width: 347.w,
                    child: NetworkImageWithLoader(hc.state.imagesList[index]),
                  );
                },
              ),
            ),
            Positioned(
              bottom: 15,
              width: MediaQuery.of(context).size.width,
              child: Align(
                alignment: Alignment.center,
                child: Container(
                  padding: const EdgeInsets.all(5),
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    color: ThemeColors.whiteColor,
                  ),
                  child: AnimatedSmoothIndicator(
                    activeIndex: pageIndicator,
                    count: hc.state.imagesList.length,
                    effect: ExpandingDotsEffect(
                      spacing: 5,
                      radius: 50.0,
                      dotWidth: 8.0,
                      dotHeight: 8.0.h,
                      activeDotColor: ThemeColors.blackColor,
                    ),
                  ),
                ),
              ),
            )
          ],
        );
      },
    );
  }
}
