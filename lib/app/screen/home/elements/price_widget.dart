import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_otel/app/core/core.dart';

class HomePriceWidget extends StatelessWidget {
  const HomePriceWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          'от 134 673 ₽',
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 30.sp,
            fontStyle: FontStyle.normal,
            color: ThemeColors.blackColor,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 12.0, left: 8),
          child: Text(
            'за тур с перелётом',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 16.sp,
              fontStyle: FontStyle.normal,
              color: ThemeColors.secondTextColor,
            ),
          ),
        ),
      ],
    );
  }
}
