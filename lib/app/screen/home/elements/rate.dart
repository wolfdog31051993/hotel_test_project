import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_otel/app/core/core.dart';

class HomeRateWidget extends StatelessWidget {
  const HomeRateWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 160.w,
      decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(5),
          ),
          color: ThemeColors.secondaryGoldColor),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5),
        child: Row(
          children: [
            const Icon(
              Icons.star,
              color: ThemeColors.goldColor,
            ),
            const SizedBox(width: 5),
            Text(
              '5 Превосходно',
              style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w500, color: ThemeColors.goldColor),
            )
          ],
        ),
      ),
    );
  }
}
