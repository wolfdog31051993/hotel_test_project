import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:test_otel/app/global/divider.dart';
import 'package:test_otel/app/global/images.dart';

import '../../../app.dart';

class HomeServiceCard extends StatelessWidget {
  const HomeServiceCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      decoration: const BoxDecoration(
        color: ThemeColors.infoContainerColor,
        borderRadius: BorderRadius.all(
          Radius.circular(5),
        ),
      ),
      child: Column(
        children: [
          infoRow(MyAssetImages.emojiHappyIc, 'Удобства'),
          const MyDivider(),
          infoRow(MyAssetImages.successIc, 'Что включено'),
          const MyDivider(),
          infoRow(MyAssetImages.closeIc, 'Что не включено'),
        ],
      ),
    );
  }

  Container infoRow(String iconString, String tittle) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        children: [
          SizedBox(
            width: 24.w,
            height: 24.h,
            child: SvgPicture.asset(iconString),
          ),
          SizedBox(width: 12.w),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                tittle,
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: ThemeColors.blackColor,
                  fontSize: 16.sp,
                ),
              ),
              Text(
                'Самое необходимое',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: ThemeColors.secondTextColor,
                  fontSize: 14.sp,
                ),
              ),
            ],
          ),
          const Spacer(),
          const Icon(
            Icons.arrow_forward_ios,
            color: ThemeColors.blackColor,
          )
        ],
      ),
    );
  }
}
