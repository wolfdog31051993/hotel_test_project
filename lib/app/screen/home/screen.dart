import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:test_otel/app/app.dart';
import 'package:test_otel/app/core/core.dart';
import 'package:test_otel/app/screen/home/elements/about_hotel.dart';
import 'package:test_otel/app/screen/home/elements/price_widget.dart';
import 'package:test_otel/app/screen/home/elements/rate.dart';
import 'package:test_otel/app/screen/home/home.dart';

import 'elements/carusel_img.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder: (hc) {
        return Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            title: Text(
              'Отель',
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18.sp, color: ThemeColors.blackColor),
              textAlign: TextAlign.center,
            ),
            centerTitle: true,
          ),
          body: SafeArea(
              child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  decoration: const BoxDecoration(
                      borderRadius:
                          BorderRadius.only(bottomLeft: Radius.circular(12), bottomRight: Radius.circular(12)),
                      color: ThemeColors.whiteColor),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const HomeCarouselImg(),
                      SizedBox(height: 21.h),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.0),
                        child: HomeRateWidget(),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 8, bottom: 16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Steigenberger Makadi',
                              style:
                                  TextStyle(fontWeight: FontWeight.w500, fontSize: 22.sp, fontStyle: FontStyle.normal),
                            ),
                            SizedBox(height: 8.h),
                            Text(
                              'Madinat Makadi, Safaga Road, Makadi Bay, Египет',
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14.sp,
                                  fontStyle: FontStyle.normal,
                                  color: ThemeColors.primaryColor),
                            ),
                            SizedBox(height: 16.0.h),
                            const HomePriceWidget(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 8.h),
                HomeAboutHotel(hc: hc),
                SizedBox(height: 8.h),
                Container(
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(12), topRight: Radius.circular(12)),
                      color: ThemeColors.whiteColor),
                  padding: const EdgeInsets.all(16.0),
                  child: PrimaryButton(
                      onPressed: () => Get.to(() => const DetailScreen(tittle: 'Steigenberger Makadi')),
                      text: 'К выбору номера'),
                )
              ],
            ),
          )),
        );
      },
    );
  }
}
