class HomeState {
  List<String> imagesList = [
    'https://steigenbergermakadionly.hotelshurghada.com/data/Pictures/OriginalPhoto/13735/1373573/1373573375/picture-hurghada-steigenberger-makadi-hotel-5.JPEG',
    'https://media-cdn.tripadvisor.com/media/photo-s/2b/2f/47/f2/steigenberger-makadi.jpg',
    'https://pics.tui.com/pics/pics1600x1200/tui/d/de406497.jpg',
    'https://pics.tui.com/pics/pics1600x1200/tui/d/de390590.jpg',
  ];

  List<String> aboutHotelList = [
    '3-я линия',
    'Платный Wi-Fi в фойе',
    '30 км до аэропорта',
    '1 км до пляжа',
  ];

  String customDesc =
      'Отель VIP-класса с собственными гольф полями. Высокий уровнь сервиса. Рекомендуем для респектабельного отдыха. Отель принимает гостей от 18 лет!';
}
