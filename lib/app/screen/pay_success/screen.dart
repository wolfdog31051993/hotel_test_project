import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:test_otel/app/app.dart';

class PaySuccessScreen extends StatelessWidget {
  const PaySuccessScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeColors.whiteColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          'Бронирование',
          style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18.sp, color: ThemeColors.blackColor),
          textAlign: TextAlign.center,
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () => Get.offAll(() => const HomeScreen()),
          icon: const Icon(
            Icons.arrow_back_ios,
            color: ThemeColors.blackColor,
          ),
        ),
      ),
      body: WillPopScope(
        onWillPop: () async {
          Get.offAll(() => const HomeScreen());
          return false;
        },
        child: Center(
          child: SizedBox(
            height: double.infinity,
            child: Column(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          padding: const EdgeInsets.all(25),
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: ThemeColors.backgroundColor,
                          ),
                          child: Image.asset(
                            MyAssetImages.successImg,
                            alignment: Alignment.center,
                            height: 44,
                          ),
                        ),
                        SizedBox(height: 32.h),
                        Text(
                          'Ваш заказ принят в работу',
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 22.sp,
                              fontStyle: FontStyle.normal,
                              color: ThemeColors.textColor),
                        ),
                        SizedBox(height: 20.h),
                        Text(
                          'Подтверждение заказа №104893 может занять некоторое время (от 1 часа до суток). Как только мы получим ответ от туроператора, вам на почту придет уведомление.',
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 16.sp,
                              fontStyle: FontStyle.normal,
                              color: ThemeColors.secondTextColor),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),
                Column(
                  children: [
                    const MyDivider(),
                    SizedBox(height: 10.h),
                    Container(
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(12), topRight: Radius.circular(12)),
                          color: ThemeColors.whiteColor),
                      padding: const EdgeInsets.all(16.0),
                      child: PrimaryButton(onPressed: () => Get.offAll(() => const HomeScreen()), text: 'Супер!'),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
