import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'app/core/theme/app_theme.dart';
import 'app/core/util/util.dart';
import 'app/data/routes/routes.dart';

Future<void> main() async {
  // debugPrint('APP NAME ============> hotel');
  runApp(const AppRoot());
}

class AppRoot extends StatelessWidget {
  const AppRoot({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HotelApp(),
    );
  }
}

class HotelApp extends StatefulWidget {
  const HotelApp({super.key});

  @override
  State<HotelApp> createState() => _HotelAppState();
}

class _HotelAppState extends State<HotelApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return DismissKeyboard(
          child: ScreenUtilInit(
            useInheritedMediaQuery: true,
            designSize: Size(constraints.maxWidth, constraints.maxHeight),
            builder: (_, child) => GetMaterialApp(
              initialRoute: AppRoutes.home,
              getPages: AppPages.list,
              debugShowCheckedModeBanner: false,
              theme: AppTheme.light,
              themeMode: ThemeMode.light,
            ),
          ),
        );
      },
    );
  }
}
